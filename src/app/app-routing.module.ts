import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FleetComponent } from './fleet/fleet.component';
import { MaintainComponent } from './maintain/maintain.component';
import { RoutesComponent } from './routes/routes.component';
import { CrewComponent } from './crew/crew.component';
import { NavigationComponent } from './navigation/navigation.component';
import { LoginComponent } from './login/login.component';
const routes: Routes = [
  {path:"fleet",component:FleetComponent},
  {path:"maintain", component:MaintainComponent},
  {path:"routes",component:RoutesComponent},
  {path:"crew",component:CrewComponent},
  {path:"navigation", component:NavigationComponent},
  {path:"login", component:LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
