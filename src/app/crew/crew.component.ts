import { Component, OnInit } from '@angular/core';
import { crew } from './crew';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-crew',
  templateUrl: './crew.component.html',
  styleUrls: ['./crew.component.css']
})
export class CrewComponent implements OnInit {

  dbUrl = "http://localhost:8080/crew";

  crew:crew;

  data:any;


  constructor(private http:HttpClient) { }

  ngOnInit() {
    this.http.get(this.dbUrl).subscribe(rs => this.data = rs);

    console.log(this.data)
  }

  insert(crewId:number,crewName:string,crewRank:string,planeId:number){


    this.crew = {

    crewId:crewId,

    crewName:crewName,

    crewRank:crewRank,

    planeId:planeId

    }

    console.log(this.crew)

    this.http.post(this.dbUrl,this.crew).subscribe();
  }

  delete(crewId:number){

    this.http.delete(this.dbUrl + "/" + crewId).subscribe();



  }

}
