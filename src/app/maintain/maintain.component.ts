import { Component, OnInit } from '@angular/core';
import { Maintenance } from './maintain';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-maintain',
  templateUrl: './maintain.component.html',
  styleUrls: ['./maintain.component.css']
})
export class MaintainComponent implements OnInit {


  databaseUrl = "http://localhost:8080/maintain";

  

  data:Maintenance;

  maintain:any

  constructor(private maintainDb:HttpClient ) { }

  ngOnInit() {

    
this.maintainDb.get(this.databaseUrl).subscribe(rs =>this.maintain = rs)
   

  }

  insert(maintenanceId:number,frameAge:number,currentMileage:number,serviceMileage:number,serviceCompany:string,planeId:number){

   this.data = { maintenanceId:maintenanceId,

    
    frameAge:frameAge,

    
    currentMileage:currentMileage,

    
    serviceMileage:serviceMileage,

   
    serviceCompany:serviceCompany,

   
    planeId:planeId
   }

   console.log(this.data)

    this.maintainDb.post(this.databaseUrl,this.data).subscribe();

    
  }

  delete(maintenanceId:number){


    this.maintainDb.delete(this.databaseUrl + "/" + maintenanceId).subscribe();


  }

}
