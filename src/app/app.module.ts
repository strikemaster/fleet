import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule} from '@angular/common/http';
import { NavigationComponent } from './navigation/navigation.component';
import { FleetComponent } from './fleet/fleet.component';
import { MaintainComponent } from './maintain/maintain.component';
import { RoutesComponent } from './routes/routes.component';
import { CrewComponent } from './crew/crew.component';
import { LoginComponent } from './login/login.component'

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    FleetComponent,
    MaintainComponent,
    RoutesComponent,
    CrewComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
