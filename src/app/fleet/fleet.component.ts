import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { planes } from './fleetModel';
@Component({
  selector: 'app-fleet',
  templateUrl: './fleet.component.html',
  styleUrls: ['./fleet.component.css']
})
export class FleetComponent implements OnInit {

  url:string = "http://localhost:8080/list";

  


  planes:Observable<any>;

  fleet:planes

  constructor(private dbconnection:HttpClient) { }

  ngOnInit() {
this.dbconnection.get<any>(this.url).subscribe(data =>this.planes = data)
console.log(this.planes)
  }

  insert(make:string,model:string,capacity:number){

   

    this.fleet ={
    
      make:make,
      model:model,
      capacity:capacity
    }

    console.log(this.fleet)

   this.dbconnection.post(this.url,this.fleet).subscribe();

  }

  delete(id:number){

  
    this.dbconnection.delete(this.url + "/" + id).subscribe();

    console.log(this.url + "/" + id)
  }



}
