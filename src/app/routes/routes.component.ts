import { Component, OnInit } from '@angular/core';
import { routes } from './routes';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-routes',
  templateUrl: './routes.component.html',
  styleUrls: ['./routes.component.css']
})
export class RoutesComponent implements OnInit {


  dbUrl = "http://localhost:8080/routes"

  routes:routes;

  data:any;

  constructor(private http:HttpClient) { }

  ngOnInit() {

    this.http.get(this.dbUrl).subscribe(rs => this.data = rs);
  }

  insert(routeId:number,routeName:string,routeDistance:number,planeId:number){

    this.routes = {

      routeId:routeId,

      routeName:routeName,
      
      routeDistance:routeDistance,
      
      planeId:planeId
      

    }

    console.log(this.routes)

   this.http.post(this.dbUrl,this.routes).subscribe();

  }

  delete(routeId:number){

   

    this.http.delete(this.dbUrl + "/" + routeId).subscribe();

    console.log(this.dbUrl + "/" + routeId)

  }

}
